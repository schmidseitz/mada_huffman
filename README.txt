Mada project Huffman
Version 1.1

This software generates the Huffman code of a *.txt file and also the Huffman code tree, by clicking on "Build Huffman code".
You can also read an external Huffman code tables with the format: <ascii code>:<Huffman code>:<frequency>-<ascii code>:<Huffman code>:<frequency>-...
The program can also read external Huffman code tables with the format: <ascii code>:<Huffman code>-<ascii code>:<Huffman code>-... but you will not have a Huffman code tree.

To compress a *.txt file, you have to click on "Zip". The Huffman code table will be saved as dec_tab.txt and the compressed text as output.dat.
Warning: All non-ACSII-Character will be replaced as '?'.

To The decompress a *.dat file, you have to click on "Unzip". The decompressed file will be saved as decompressed.txt.


Developed May 2016 by Steven Schmied and Claudio Seitz
