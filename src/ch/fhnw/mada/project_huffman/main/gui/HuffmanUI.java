package ch.fhnw.mada.project_huffman.main.gui;

import ch.fhnw.mada.project_huffman.main.data.HuffmanCharacter;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * Created by Claudio on 08.05.2016.
 */
public class HuffmanUI extends GridPane {
    private HuffmanModel model;

    private Label file;
    private TextField path;
    private Button selectFile;
    private Button zip;
    private Button buildHuffmanCode;
    private Button unzip;
    private TabPane tabPane;
    private TreeView<HuffmanCharacter> tree;
    private TableView<HuffmanCharacter> table;

    public HuffmanUI(HuffmanModel model) {
        this.model = model;
        initializeControls();
        layoutControls();
        addEventHandlers();
        addBindings();
    }

    public void initializeControls(){
        file = new Label("File");
        path = new TextField();
        selectFile = new Button("...");
        zip = new Button("zip");
        buildHuffmanCode = new Button("Build Huffman code");
        unzip = new Button("unzip");
        tabPane = new TabPane();
        tree = new TreeView<>();
        table = new TableView<>();
    }

    public void layoutControls(){
        setPadding(new Insets(5));
        setHgap(5);
        setVgap(5);

        ColumnConstraints sliderColumn = new ColumnConstraints();
        sliderColumn.setHgrow(Priority.ALWAYS);
        getColumnConstraints().addAll(sliderColumn);

        addRow(0, file);
        add(path, 0, 1, 2, 1);
        add(selectFile, 2, 1, 1, 1);
        addRow(2, buildHuffmanCode, zip, unzip);
        add(tabPane, 0, 3, 3, 1);

        tabPane.getTabs().addAll(new Tab("Tree"), new Tab("Table"));
        tabPane.getTabs().get(0).setContent(tree);
        tabPane.getTabs().get(1).setContent(table);

        TableColumn<HuffmanCharacter, String> columnCharacter = new TableColumn<>("Char");
        columnCharacter.setMinWidth(40);
        columnCharacter.setPrefWidth(40);

        TableColumn<HuffmanCharacter, String> columnHuffmanCode = new TableColumn<>("Huffman code");
        columnHuffmanCode.setMinWidth(100);
        columnHuffmanCode.setPrefWidth(120);

        TableColumn<HuffmanCharacter, String> columnFrequency = new TableColumn<>("Frequency");
        columnFrequency.setMinWidth(70);
        columnFrequency.setPrefWidth(70);

        table.getColumns().addAll(columnCharacter, columnHuffmanCode, columnFrequency);
    }

    public void addEventHandlers(){
        selectFile.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                model.chooseFile();
            }
        });
        buildHuffmanCode.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    clearTree();
                    buildTree(model.buildCodeFromFile());
                } catch (Exception ex){
                    //do nothing
                }
            }
        });
        zip.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                model.zipFile();
            }
        });
        unzip.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                model.unzipFile();
            }
        });
    }

    public void addBindings() {
        model.getPathProperty().bindBidirectional(path.textProperty());

        table.setItems(model.getList());
        ((TableColumn<HuffmanCharacter, String>) table.getColumns().get(0)).setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        ((TableColumn<HuffmanCharacter, String>) table.getColumns().get(1)).setCellValueFactory(cellData -> cellData.getValue().huffmannCodeProperty());
        ((TableColumn<HuffmanCharacter, String>) table.getColumns().get(2)).setCellValueFactory(cellData -> cellData.getValue().frequencyProperty().asString());
    }

    /**
     * Clears the tree view
     */
    public void clearTree() {
        tree.setRoot(null);
    }

    /**
     * Builds the Huffman code tree in the TreeView
     * @param root root of the Huffman code tree
     */
    public void buildTree(HuffmanCharacter root) {
        TreeItem<HuffmanCharacter> rootNode = new TreeItem<>(root);
        tree.setRoot(rootNode);
        buildTreeChildren(root.getLeft0(), rootNode);
        buildTreeChildren(root.getRight1(), rootNode);
    }

    /**
     * Parses the Huffman code tree and copy to the TreeView
     * Recursive method
     * @param character Huffman character node
     * @param parent TreeItem parent node
     */
    private void buildTreeChildren(HuffmanCharacter character, TreeItem<HuffmanCharacter> parent) {
        TreeItem<HuffmanCharacter> itemNode = new TreeItem<>(character);
        parent.getChildren().add(itemNode);

        if(!character.isLeaf()) {
            buildTreeChildren(character.getLeft0(), itemNode);
            buildTreeChildren(character.getRight1(), itemNode);
        }
    }
}
