package ch.fhnw.mada.project_huffman.main.gui;

import ch.fhnw.mada.project_huffman.main.data.*;
import com.sun.istack.internal.NotNull;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.Arrays;
import java.util.IllegalFormatCodePointException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Claudio on 08.05.2016.
 */
public class HuffmanModel {
    private String currentDirectory;
    private String[] fileNames = {"dec_tab.txt", "output.dat", "decompress.txt"};
    private StringBuilder text;
    private HuffmanCodeBuilder codeBuilder;
    private CompressionHandler compressionHandler;
    private HuffmanCharacter root;
    private StringProperty path;
    private ObservableList<HuffmanCharacter> list;

    public HuffmanModel(){
        this.currentDirectory = System.getProperty("user.dir");
        this.path = new SimpleStringProperty();
        this.list = FXCollections.observableArrayList();
        this.text = new StringBuilder();
    }

    public void setPath(String path) {
        this.path.setValue(path);
    }

    public String getPath(){
        return this.path.getValue();
    }

    public StringProperty getPathProperty() {
        return path;
    }

    /**
     * Fills the observable list for showing in the table view
     * @param characterArray
     */
    public void fillList (@NotNull HuffmanCharacter[] characterArray) {
        Arrays.stream(characterArray).filter(c -> c.getHuffmannCode().length() > 0).forEach(character -> {
            if (! list.contains(character)){
                list.add(character);
            }
        });
    }

    public ObservableList<HuffmanCharacter> getList() {
        return list;
    }

    /**
     * Choose a .txt or a .dat file
     */
    public void chooseFile() {
        try{
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select text or datafile");
            fileChooser.setInitialDirectory(new File(currentDirectory));
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text file or Dat file", "*.txt", "*.dat"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                setPath(selectedFile.getPath());
            }
        }
        catch (Exception ex) {
            printErrorMessage(ex);
        }
    }

    /**
     * Builds the Huffman code tree from a normal text file
     * Or builds the Huffman code tree from an external code tree text file
     * Or fills the Huffman code table from an external text file, no treeView and no frequency available
     * @return root of the Huffman code tree
     */
    public HuffmanCharacter buildCodeFromFile() {
        File file = new File(getPath());
        if (file.exists()) {
            try {
                if (!getPath().endsWith(".txt")) {
                    throw  new IllegalArgumentException("Huffman code can only be imported from .txt files");
                }
                list.clear();
                text.delete(0, text.length());
                codeBuilder = new HuffmanCodeBuilder();
                root = null;
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getPath()))) {
                    bufferedReader.lines().forEach(line -> {
                        //replace non-ascii
                        for(char c : line.toCharArray()) {
                            if ((int)c > 127) {
                                line = line.replace(c, '?');
                            }
                        }
                        text.append(line);
                        text.append("\n");
                    });
                    text.deleteCharAt(text.length() - 1);
                    String pattern = "(\\d+:\\d+:\\d+)(" + Pattern.quote("-") + "(\\d+:\\d+:\\d+))*";
                    String pattern2 = "(\\d+:\\d+)(" + Pattern.quote("-") + "(\\d+:\\d+))*";
                    if (text.toString().matches(pattern)) {
                        //external table
                        String[] rows = text.toString().split(Pattern.quote("-"));
                        for (int i = 0; i < rows.length; i++) {
                            String[] fields = rows[i].split(":");
                            codeBuilder.getTable()[new Integer(fields[0])].setFrequency(new Integer(fields[2]));
                        }
                        root = codeBuilder.buildHuffmanCharacterTree();
                        root.setHuffmannCode("Empty");
                        text.delete(0, text.length());
                        setPath("");
                    } else if (text.toString().matches(pattern2)) {
                        //external table without tree
                        //no tree and no frequency available
                        String[] rows = text.toString().split(Pattern.quote("-"));
                        for (int i = 0; i < rows.length; i++) {
                            String[] fields = rows[i].split(":");
                            codeBuilder.getTable()[new Integer(fields[0])].setHuffmannCode(fields[1]);
                        }
                        text.delete(0, text.length());
                        setPath("");
                    } else {
                        // normal text file
                        codeBuilder.countCharacter(text.toString());
                        root = codeBuilder.buildHuffmanCharacterTree();
                        root.setHuffmannCode("Empty");
                    }
                    fillList(codeBuilder.getTable());
                } catch (Exception ex) {
                    printErrorMessage(ex);
                }
            } catch (Exception ex) {
                printErrorMessage(ex);
            }
        }
        return root;
    }

    /**
     * Compresses a text file into output.dat
     * Uses the Huffman code table
     * The Huffman code tree is saved as dec_tab.txt
     * (The Frequency will be saved as well to build the code tree from this file)
     */
    public void zipFile() {
        try {
            if (codeBuilder == null) {
                throw new NullPointerException("No Huffman code available");
            }
            if (text.length() == 0) {
                if (getPath().endsWith(".txt")) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Warning Dialog");
                    alert.setHeaderText(null);
                    alert.setContentText("No text available.\n" +
                            "Do you want to load file: " + getPath() + "?\n" +
                            "Warning: The current Huffman code will be lost.");
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK){
                        buildCodeFromFile();
                    }
                } else {
                    throw  new IllegalArgumentException("No text to compress");
                }
            }
            File treeFile = createOverrideFile(currentDirectory + "\\" + fileNames[0]);
            File zippedTextFile = createOverrideFile(currentDirectory + "\\" + fileNames[1]);
            if (treeFile != null && zippedTextFile != null) {
                //treeFile
                try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(treeFile.getPath()))){
                    HuffmanCharacter[] table = Arrays.stream(codeBuilder.getTable()).filter(c ->  c.getFrequency() > 0).toArray(HuffmanCharacter[]::new);
                    String[] codeString = new String[table.length];
                    for (int i = 0; i <  table.length; i++) {
                        codeString[i] = (int) table[i].getName().charAt(0) + ":" + table[i].getHuffmannCode() + ":" + table[i].getFrequency();
                    }
                    bufferedWriter.write(String.join("-", codeString));
                } catch (Exception ex) {
                        printErrorMessage(ex);
                }
                //textFile
                compressionHandler = new CompressionHandler(codeBuilder.getTable(), root);
                try (FileOutputStream fos = new FileOutputStream(zippedTextFile.getPath())){
                    fos.write(compressionHandler.zip(text.toString()));
                } catch (Exception ex) {
                    printErrorMessage(ex);
                }
            }
            text.delete(0, text.length());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText(getPath() + " compression successful.\n"
                    + "Compressed file: " + zippedTextFile.getPath() +"\n"
                    + "Tree file: " + treeFile.getPath());
            alert.showAndWait();
        } catch (Exception ex) {
            printErrorMessage(ex);
        }
    }

    /**
     * Decompresses a .dat file into decompress.txt
     * Uses the Huffman code tree, if the Huffman code tree is available
     * Otherwise it uses the Huffman code table with linear search
     */
    public void unzipFile() {
        try {
            File zippedFile = new File(getPath());
            File unzippedFile = createOverrideFile(currentDirectory + "\\" + fileNames[2]);
            if (unzippedFile != null) {
                if (!getPath().endsWith(".dat")) {
                    throw  new IllegalArgumentException("Only .dat file can be decompressed");
                }
                if (codeBuilder == null) {
                    throw new NullPointerException("No Huffman code to decompress");
                }
                compressionHandler = new CompressionHandler(codeBuilder.getTable(), root);
                byte[] bFile = new byte[(int) zippedFile.length()];
                try (FileInputStream fis = new FileInputStream(zippedFile.getPath())) {
                    fis.read(bFile);
                } catch (Exception ex) {
                    printErrorMessage(ex);
                }
                String decompressText;
                if (root != null) {
                    decompressText = compressionHandler.unzip(bFile);
                } else {
                    decompressText = compressionHandler.unzipLinear(bFile);
                }
                try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(unzippedFile.getPath()))) {
                    bufferedWriter.write(decompressText);
                } catch (Exception ex) {
                    printErrorMessage(ex);
                }
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText(getPath() + " decompression successful.\n"
                    + "Decompressed file: " + unzippedFile.getPath());
            alert.showAndWait();
        } catch (Exception ex) {
            printErrorMessage(ex);
        }
    }

    /**
     * Creates a new file or you can override it
     * @param path
     * @return file
     */
    private File createOverrideFile (String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.createNewFile();
                return file;
            } catch (Exception ex) {
                throw new NullPointerException("File cannot be created");
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Warning Dialog");
            alert.setHeaderText(null);
            alert.setContentText("File: " + file.getPath() + " already exists."
                    + "Do you want to override this file?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                return file;
            } else {
                return null;
            }
        }
    }

    public void printErrorMessage(Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(null);
        alert.setContentText(ex.getMessage());
        alert.showAndWait();
    }
}
