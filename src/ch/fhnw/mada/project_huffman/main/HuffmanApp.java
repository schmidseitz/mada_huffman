package ch.fhnw.mada.project_huffman.main;

import ch.fhnw.mada.project_huffman.main.gui.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Claudio on 08.05.2016.
 */
public class HuffmanApp extends Application{
    private HuffmanModel model;
    private HuffmanUI ui;
    private Scene scene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        model = new HuffmanModel();
        ui = new HuffmanUI(model);
        scene = new Scene(ui);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Huffman code");
        primaryStage.show();
    }
}
