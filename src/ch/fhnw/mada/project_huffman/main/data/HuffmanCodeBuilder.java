package ch.fhnw.mada.project_huffman.main.data;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Steven on 08.05.16.
 */
public class HuffmanCodeBuilder {
    private HuffmanCharacter[] table;

    public HuffmanCodeBuilder() {
        table = new HuffmanCharacter[128];
        for (int i = 0; i < 128; i++) {
            table[i] = new HuffmanCharacter((char) i);
        }
    }

    /**
     * Counts up the frequency of each Huffman character from the string
     * @param string
     */
    public void countCharacter (String string) {
        string.chars().forEach(c -> table[c].increment());
    }

    /**
     * Builds the Huffman code tree by combining always two Huffman characters with the lowest frequency
     * @return root of the Hoffman code tree
     */
    public HuffmanCharacter buildHuffmanCharacterTree() {
        List<HuffmanCharacter> codeList = Arrays.stream(table).filter(c -> c.getFrequency() > 0).collect(Collectors.toList());
        while(codeList.size() > 1) {
            codeList.sort(HuffmanCharacter::compareTo);
            HuffmanCharacter character1 = codeList.get(0);
            HuffmanCharacter character2 = codeList.get(1);
            codeList.add(new HuffmanCharacter(character1, character2));
            codeList.remove(character1);
            codeList.remove(character2);
        }
        return codeList.iterator().next();
    }

    public HuffmanCharacter[] getTable() {
        return table;
    }
}
