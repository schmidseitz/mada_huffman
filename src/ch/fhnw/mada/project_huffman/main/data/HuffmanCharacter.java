package ch.fhnw.mada.project_huffman.main.data;

import com.sun.istack.internal.NotNull;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Steven on 08.05.16.
 */
public class HuffmanCharacter implements Comparable<HuffmanCharacter> {
    private StringProperty name;
    private StringProperty huffmannCode;
    private IntegerProperty frequency;
    private HuffmanCharacter left0;
    private HuffmanCharacter right1;

    public String getName() {
        return name.get();
    }
    public StringProperty nameProperty() {
        return name;
    }
    public void setName(String name) {
        this.name.set(name);
    }

    public String getHuffmannCode() {
        return huffmannCode.get();
    }
    public StringProperty huffmannCodeProperty() {
        return huffmannCode;
    }
    public void setHuffmannCode(String huffmannCode) {
        this.huffmannCode.set(huffmannCode);
    }

    public int getFrequency() {
        return frequency.get();
    }
    public IntegerProperty frequencyProperty() {
        return frequency;
    }
    public void setFrequency(int frequency) {
        this.frequency.set(frequency);
    }

    public HuffmanCharacter getRight1() {
        return right1;
    }
    public HuffmanCharacter getLeft0() {
        return left0;
    }

    private HuffmanCharacter () {
        this.name = new SimpleStringProperty("");
        this.huffmannCode = new SimpleStringProperty("");
        this.frequency = new SimpleIntegerProperty(0);
    }

    /**
     * Generates a new Huffman character form a ascii char
     * @param ascii
     */
    public HuffmanCharacter(@NotNull char ascii) {
        this();
        this.setName("" + ascii);
    }

    /**
     * Generates a new Huffman character from two combined Huffman characters
     * Generates also a part of the Huffman code tree
     * Extends the Huffman code of each parameter and children of each parameter
     * @param left becomes the left0 child of the new character
     * @param right becomes the right1 child of the new character
     */
    public HuffmanCharacter(@NotNull HuffmanCharacter left, @NotNull HuffmanCharacter right) {
        this();
        this.left0 = left;
        this.right1 = right;
        this.setFrequency(left0.getFrequency() + right1.getFrequency());
        this.setName(left0.getName() + right1.getName());
        extendHuffmanCode(left0, "0");
        extendHuffmanCode(right1, "1");
    }

    /**
     *
     * @return true, if this Huffman character has no children
     */
    public boolean isLeaf() {
        return this.left0 == null || this.right1 == null;
    }

    /**
     * Extends the Huffman code of itself and each child
     * Recursive method
     * @param character
     * @param code
     */
    private void extendHuffmanCode(HuffmanCharacter character, String code) {
        character.setHuffmannCode(code + character.getHuffmannCode());
        if(!character.isLeaf()) {
            extendHuffmanCode(character.getLeft0(), code);
            extendHuffmanCode(character.getRight1(), code);
        }
    }

    /**
     * increments the frequency
     */
    public void increment() {
        setFrequency(getFrequency() + 1);
    }

    /**
     * Ordering by frequency
     * @param character
     * @return int value to compare
     */
    @Override
    public int compareTo(HuffmanCharacter character) {
        return this.getFrequency() - character.getFrequency();
    }

    @Override
    public String toString() {
        return "Character: " + getName() +", HuffmannCode: " + getHuffmannCode() + ", Frequency: " + getFrequency();
    }
}
