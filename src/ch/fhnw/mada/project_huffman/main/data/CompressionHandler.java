package ch.fhnw.mada.project_huffman.main.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Claudio on 11.05.2016.
 */
public class CompressionHandler {
    private HuffmanCharacter[] table;
    private HuffmanCharacter root;

    public CompressionHandler(HuffmanCharacter[] table, HuffmanCharacter root) {
        this.table = table;
        this.root = root;
    }

    /**
     * Compresses a string into a byte array
     * Uses the Huffman code
     * @param string string to compress
     * @return compressed byte array
     */
    public byte[] zip(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        string.chars().forEach(intChar -> stringBuilder.append(table[intChar].getHuffmannCode()));
        stringBuilder.append(1);
        int moduloLength =  8 - (stringBuilder.length() % 8);
        for (int i = 0; i < moduloLength; i++) {
            stringBuilder.append(0);
        }
        int numOfBytes = stringBuilder.length() / 8;
        byte[] bytes = new byte[numOfBytes];
        for (int i = 0; i < numOfBytes; i++){
            bytes[i] = binaryStringToByte(stringBuilder.substring(i * 8, (i + 1) * 8));
        }
        return bytes;
    }

    /**
     * Decompresses a byte array into a string
     * Uses the Huffman code tree
     * @param bytes compressed byte array
     * @return decompressed string
     */
    public String unzip(byte[] bytes) {
        StringBuilder binaryString = new StringBuilder();
        for (byte b: bytes) {
            binaryString.append(byteToBinaryString(b));
        }
        int stringEnd = binaryString.length() - 1;
        while (binaryString.charAt(stringEnd) == '0') {
            stringEnd--;
        }
        binaryString.delete(stringEnd, binaryString.length());
        return parseTree(binaryString.toString());
    }

    /**
     * Parses the Huffman code tree
     * @param binaryString
     * @return decompressed string
     */
    private String parseTree(String binaryString) {
        StringBuilder stringBuilder = new StringBuilder();
        HuffmanCharacter currentNode = root;
        for (int i = 0; i < binaryString.length(); i++) {
            if (!currentNode.isLeaf()) {
                currentNode = getNextHuffmanCharacter(currentNode, binaryString.charAt(i));
            } else {
                stringBuilder.append(currentNode.getName());
                currentNode = getNextHuffmanCharacter(root, binaryString.charAt(i));
            }
        }
        stringBuilder.append(currentNode.getName());
        return stringBuilder.toString();
    }

    /**
     * Gets next node or leaf
     * '0' returns the left0 child of character
     * '1' returns the right1 child of character
     * @param character Huffman code tree node
     * @param binaryChar '0' or '1'
     * @return Huffman code tree node or leaf
     */
    private HuffmanCharacter getNextHuffmanCharacter(HuffmanCharacter character, char binaryChar) {
        if (binaryChar == '0') {
            return character.getLeft0();
        } else {
            return character.getRight1();
        }
    }

    /**
     * Decompresses a byte array into a string
     * Uses a Huffman code table and linear search
     * @param bytes compressed byte array
     * @return decompressed string
     */
    public String unzipLinear(byte[] bytes) {
        StringBuilder binaryString = new StringBuilder();
        for (byte b : bytes) {
            binaryString.append(byteToBinaryString(b));
        }
        int stringEnd = binaryString.length() - 1;
        while (binaryString.charAt(stringEnd) == '0') {
            stringEnd--;
        }
        binaryString.delete(stringEnd, binaryString.length());

        List<String> huffmanCodes = new ArrayList<>();
        for (HuffmanCharacter character : table) {
            huffmanCodes.add(character.getHuffmannCode());
        }
        StringBuilder stringBuilder = new StringBuilder();
        int startCodeIndex = 0;
        for (int i = 1; i < binaryString.length(); i++) {
            String code = binaryString.substring(startCodeIndex, i);
            if (huffmanCodes.contains(code)) {
                int n = 0;
                while (!code.equals(huffmanCodes.get(n))) {
                    n++;
                }
                stringBuilder.append((char) n);
                startCodeIndex = i;
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Converts an byte value into a binary string
     * @param b
     * @return binary string
     */
    private String byteToBinaryString(byte b) {
        StringBuilder sb = new StringBuilder();
        for (int i = 7; i >= 0; --i) {
            sb.append(b >>> i & 1);
        }
        return sb.toString();
    }

    /**
     * Converts a binary string into a byte value
     * @param binaryString
     * @return byte value
     */
    private byte binaryStringToByte(String binaryString) {
        byte b;
        if (binaryString.charAt(0) == '0') {
            b = Byte.parseByte(binaryString, 2);
        } else {
            //negative byte
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 8; i++){
                if (binaryString.charAt(i) == '0'){
                    sb.append("1");
                } else {
                    sb.append("0");
                }
            }
            b = Byte.parseByte(sb.toString(), 2);
            b ++;
            b *= -1;
        }
        return b;
    }
}
